const { scheduleJob } = require('../common/scheduleJob');
const { getCronRule } = require('../common/getCronRule');
const asyncHandler = require('express-async-handler');
const { v4: uuidv4 } = require('uuid');
const nodeSchedule = require('node-schedule');
const { cancelJob } = require('../common/cancelJob');
const _ = require('lodash');
const { validationResult } = require('express-validator');
const logger = require("../common/logger");
const { requestAuditUpdate } = require("../common/updateOperations");

/**
 * scheduleController.js
 *
 * @description :: Server-side logic for managing schedules.
 */
module.exports = {
    /**
     * scheduleController.list()
     */
    list: asyncHandler(async (req, res, next) => {
        if (req.body.status === 'deleted') {
            return res.json({
                statusCode: 200,
                schedulesCount: 0,
                data: [],
                success: true
            });
        }

        // @ts-ignore
        const scheduleModel = await req.db.model('schedule');

        let filter = {
            clientId: req.body.clientId,
            status: { $in: ['active', 'inActive'] },
        };

        if (!_.isEmpty(req.body.status)) {
            filter.status = req.body.status;
        }
        let schedules = await scheduleModel.find(filter, { _id: 0, __v: 0 }).exec();
        return res.json({
            statusCode: 200,
            schedulesCount: schedules.length,
            data: schedules,
            success: true
        });
    }),

    /**
     * scheduleController.create()
     */
    create: asyncHandler(async (req, res, next) => {
        /**
         * Validation
         */
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            return res.status(400).json({ errors: errors.array() });
        }

        let processTime = new Date();

        // @ts-ignore
        let rrId = req.rrId;

        // @ts-ignore
        let solutionName = req.solutionName;

        // set recurrence = true as default value.
        const recurrence = req.body.recurrence === false ? false : true;
        let scheduleRule;
        try {
            scheduleRule = await getCronRule({
                ...req.body,
                recurrence,
            });
        } catch (e) {
            // update requestAudit collection
            await requestAuditUpdate({
                clientId: req.body.clientId, rrId, solutionName,
                requestBody: { ...req.body, recurrence }, processTime, status: "failed",
                remark: JSON.stringify(e.message)
            });
            logger.info(rrId, solutionName, e.message, {
                clientId: req.body.clientId,
                processTime, status: "failed"
            });
            return next(e);
        }

        let job;
        try {
            // schedule the job
            job = await scheduleJob({
                ...req.body,
                jobId: uuidv4(),
                recurrence,
                scheduleRule,
                rrId,
                solutionName
            });
        } catch (e) {
            // update requestAudit collection
            await requestAuditUpdate({
                clientId: req.body.clientId, rrId, solutionName,
                requestBody: { ...req.body, recurrence }, processTime, status: "failed",
                remark: JSON.stringify(e.message)
            });
            logger.info(rrId, solutionName, e.message, {
                clientId: req.body.clientId,
                processTime, status: "failed"
            });
            return next(e);
        }

        // If no job then return to client
        if (!job) {
            let message = `No job created for the given schedule time, Check whether time correct and should be in future`;
            // update requestAudit collection
            await requestAuditUpdate({
                clientId: req.body.clientId, rrId, solutionName,
                requestBody: { ...req.body, recurrence }, processTime, status: "failed",
                remark: JSON.stringify(message)
            });
            logger.info(rrId, solutionName, message, {
                clientId: req.body.clientId,
                 processTime, status: "failed"
            });
            return res.status(500).json({
                success: false,
                statusCode: 500,
                message
            });
        }

        const dynamicScheduleData = {
            scheduleId: uuidv4(),
            jobId: job.name,
            createdTime: new Date(),
            status: "active",
            recurrence
        };


        if (_.has(req, "headers.userSession.userId")) {
            // @ts-ignore
            dynamicScheduleData.createdBy = req.headers.userSession.userId;
        } else if (_.has(req, "body.userSession.userId")) {
            dynamicScheduleData.createdBy = req.body.userSession.userId;
        } else {
            dynamicScheduleData.createdBy = req.body.requestingServiceName;
        }

        // Save the record to db
        // @ts-ignore
        const scheduleModel = await req.db.model('schedule');
        const schedule = new scheduleModel({ ...req.body, ...dynamicScheduleData });
        schedule.save(async function (err, schedule) {
            if (err) {
                let message = `Error when creating schedule (i.e., in save())`;
                // update requestAudit collection
                await requestAuditUpdate({
                    clientId: req.body.clientId, rrId, solutionName,
                    requestBody: { ...req.body, recurrence }, processTime, status: "failed",
                    remark: JSON.stringify(message)
                });
                logger.info(rrId, solutionName, message, {
                    clientId: req.body.clientId,
                    processTime, status: "failed"
                });
                return res.status(500).json({
                    success: false,
                    statusCode: 500,
                    message: message,
                    error: err,
                });
            }

            let message = `Successfully created the schedule`;
            // update requestAudit collection
            await requestAuditUpdate({
                clientId: req.body.clientId, rrId, solutionName,
                requestBody: { ...req.body, recurrence }, processTime, status: "success",
                remark: JSON.stringify(message)
            });
            logger.info(rrId, solutionName, message, {
                clientId: req.body.clientId,
                processTime, status: "success"
            });
            return res.status(201).json({
                success: true,
                statusCode: 201,
                message: message,
                scheduleId: schedule.scheduleId,
            });
        });
    }),

    /**
     * scheduleController.update()
     */
    update: asyncHandler(async (req, res, next) => {

        /**
         * Validation
         */
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            return res.status(400).json({ errors: errors.array() });
        }

        // get the schedule model instance
        // @ts-ignore
        const scheduleModel = await req.db.model('schedule');

        // Find a schedule with given 3 mandatory parameters
        scheduleModel.findOne(
            {
                scheduleId: req.body.scheduleId,
                clientId: req.body.clientId,
                requestingServiceName: req.body.requestingServiceName,
                status: { $ne: "deleted" }
            },
            async (err, schedule) => {
                if (err) {
                    return res.status(500).json({
                        message:
                            'Error when getting schedule with given clientId, scheduleId, requestingServiceName',
                        error: err,
                    });
                }
                if (!schedule) {
                    return res.status(404).json({
                        message: 'No such schedule',
                    });
                }

                let processTime = new Date();

                if (_.has(req, "headers.userSession.userId")) {
                    // @ts-ignore
                    schedule.updatedBy = req.headers.userSession.userId;
                } else if (_.has(req, "body.userSession.userId")) {
                    schedule.updatedBy = req.body.userSession.userId;
                } else {
                    schedule.updatedBy = req.body.requestingServiceName;
                }

                // @ts-ignore
                let rrId = req.rrId;

                // @ts-ignore
                let solutionName = req.solutionName;

                schedule.updatedTime = new Date();

                // if req.body.status === inActive then delete the job for the given id and update the record
                if (req.body.status === 'inActive') {
                    const result = await cancelJob({
                        jobId: schedule.jobId,
                    });
                    // If result === null or result = deleted means (job has been already stopped) or (stopped)
                    // so the udpate status to inActive
                    if (!result || result.deleted) {
                        // if result is null or deleted then update the schedule with status = inActive
                        schedule.status = "inActive";

                        return schedule.save(async function (err, schedule) {
                            if (err) {
                                let message = `Error when updating schedule(in save() PUT)`;
                                // update requestAudit collection
                                await requestAuditUpdate({
                                    clientId: req.body.clientId, rrId, solutionName,
                                    requestBody: { ...req.body }, processTime, status: "failed",
                                    remark: JSON.stringify(message)
                                });
                                logger.info(rrId, solutionName, message, {
                                    clientId: req.body.clientId,
                                    processTime, status: "failed",
                                });
                                return res.status(500).json({
                                    message: message,
                                    error: err,
                                });
                            }
                            let message = `Successfully updated the schedule`;
                            // update requestAudit collection
                            await requestAuditUpdate({
                                clientId: req.body.clientId, rrId, solutionName,
                                requestBody: { ...req.body }, processTime, status: "failed",
                                remark: JSON.stringify(message)
                            });
                           
                            logger.info(rrId, solutionName, message, {
                                clientId: req.body.clientId,
                                processTime, status: "success",
                            });
                            return res.status(201).json({
                                message: message,
                                scheduleId: schedule.scheduleId,
                            });
                        });
                    } else {
                        let message = `Updating the schedule failed`;
                        // update requestAudit collection
                        await requestAuditUpdate({
                            clientId: req.body.clientId, rrId, solutionName,
                            requestBody: { ...req.body }, processTime, status: "failed",
                            remark: `${message}, ${result.message}`
                        });
                        logger.info(rrId, solutionName, `${message}, ${result.message}`, {
                            clientId: req.body.clientId,
                            processTime, status: "failed"
                        });
                        return res.status(500).json({
                            message: message,
                            error: new Error(result.message),
                        });
                    }
                } else {
                    // This block handles all scenarios of where req.body.status is undefined or null or not equal to inActive or anything else

                    let newScheduleData = { ...req.body };

                    let scheduleRule;
                    try {
                        // Get the scheduleRule
                        scheduleRule = await getCronRule({
                            ...schedule._doc,
                            ...req.body
                        });
                    } catch (e) {
                        // update requestAudit collection
                        await requestAuditUpdate({
                            clientId: req.body.clientId, rrId, solutionName,
                            requestBody: { ...req.body }, processTime, status: "failed",
                            remark: e.message
                        });
                        logger.info(rrId, solutionName, `${e.message}`, {
                            clientId: req.body.clientId,
                             processTime, status: "failed"
                        });
                        return next(e);
                    }

                    // Get the job and cancel it and again schedule it to update with latest data
                    let oldJob = nodeSchedule.scheduledJobs[schedule.jobId];

                    if (oldJob) {
                        if (!oldJob.cancel()) {
                            let message = `Could not cancel the job due to some techinal issue`;
                            // update requestAudit collection
                            await requestAuditUpdate({
                                clientId: req.body.clientId, rrId, solutionName,
                                requestBody: { ...req.body }, processTime, status: "failed",
                                remark: message
                            });
                            logger.info(rrId, solutionName, message, {
                                clientId: req.body.clientId,
                                 processTime, status: "failed"
                            });
                            return res.status(500).json({
                                message
                            });
                        }
                    }

                    let job;
                    try {
                        // when update request comes then schedule new job with new jobId
                        newScheduleData.jobId = uuidv4();
                        job = await scheduleJob({
                            ...schedule._doc, ...req.body, scheduleRule, jobId: newScheduleData.jobId, rrId, solutionName
                        });
                    } catch (e) {
                        // update requestAudit collection
                        await requestAuditUpdate({
                            clientId: req.body.clientId, rrId, solutionName,
                            requestBody: { ...req.body }, processTime, status: "failed",
                            remark: e.message
                        });
                        logger.info(rrId, solutionName, e.message, {
                            clientId: req.body.clientId,
                             processTime, status: "failed"
                        });
                        return next(e);
                    }

                    if (!job) {
                        let message = `Scheduling of a job failed`;
                        // update requestAudit collection
                        await requestAuditUpdate({
                            clientId: req.body.clientId, rrId, solutionName,
                            requestBody: { ...req.body }, processTime, status: "failed",
                            remark: message
                        });
                        logger.info(rrId, solutionName, message, {
                            clientId: req.body.clientId,
                             processTime, status: "failed"
                        });
                        return res.status(500).json({
                            message: message,
                            error: message,
                        });
                    } else {
                        _.forEach(newScheduleData, (v, k) => {
                            schedule[k] = v;
                        });
                        schedule.status = 'active';
                        return schedule.save(async function (err, schedule) {
                            if (err) {
                                // update requestAudit collection
                                await requestAuditUpdate({
                                    clientId: req.body.clientId, rrId, solutionName,
                                    requestBody: { ...req.body }, processTime, status: "failed",
                                    remark: err.message
                                });
                                logger.info(rrId, solutionName, err.message, {
                                    clientId: req.body.clientId,
                                    processTime, status: "failed"
                                });
                                return res.status(500).json({
                                    message: 'Error when updating schedule.',
                                    error: err,
                                });
                            }

                            let message = `Successfully updated the schedule`;
                            // update requestAudit collection
                            await requestAuditUpdate({
                                clientId: req.body.clientId, rrId, solutionName,
                                requestBody: { ...req.body }, processTime, status: "success",
                                remark: message
                            });
                            logger.info(rrId, solutionName, message, {
                                clientId: req.body.clientId,
                                processTime, status: "success"
                            });
                            return res.status(201).json({
                                message,
                                scheduleId: schedule.scheduleId,
                            });
                        });
                    }
                }
            },
        );
    }),

    /**
     * scheduleController.remove()
     */
    remove: asyncHandler(async (req, res, next) => {
        // validate scheduleIds array
        if (
            !_.has(req, 'body.scheduleIds') ||
            _.isEmpty(req.body.scheduleIds)
        ) {
            let message = `Pass a valid scheduleId(s) in body params`;
            // @ts-ignore
            logger.info(req.rrId, req.solutionName, message);
            throw new Error('Pass a valid scheduleId(s) in body params');
        }

        const scheduleIds = _.isArray(req.body.scheduleIds)
            ? req.body.scheduleIds
            : [req.body.scheduleIds];

        // Get the jobIds using scheduleIds
        // @ts-ignore
        let scheduleModel = await req.db.model('schedule');
        let schedules = await scheduleModel
            .find({
                clientId: req.body.clientId,
                scheduleId: { $in: scheduleIds },
            })
            .exec();

        let idsToBeDeleted = [];
        // write synchronous for loop
        for (const schedule of schedules) {
            // destroy the job
            const result = await cancelJob({
                jobId: schedule.jobId,
            });
            if (!result || result.deleted) {
                idsToBeDeleted.push(schedule.scheduleId);
            }
        }

        let updatedBy;
        if (_.has(req, 'headers.userSession.userId')) {
            // @ts-ignore
            updatedBy = req.headers.userSession.userId;
        } else if (_.has(req, 'body.userSession.userId')) {
            // @ts-ignore
            updatedBy = req.body.userSession.userId;
        } else {
            updatedBy = req.body.requestingServiceName;
        }

        let result = await scheduleModel.updateMany(
            {
                clientId: req.body.clientId,
                scheduleId: { $in: idsToBeDeleted },
            },
            { $set: { status: 'deleted', updatedBy, updatedTime: new Date() } },
        );

        // @ts-ignore
        logger.info(req.rrId, req.solutionName, `scheduleModel.updateMany ${JSON.stringify(result)}`);
        /**
         * {
            "n": 1,
            "nModified": 1,
            "ok": 1
        }
         */
        res.status(202).json({
            count: result.n,
            deleted: result.nModified,
            ok: result.ok,
            message: "Successfully deleted the schedule(s)"
        });
    }),
};
