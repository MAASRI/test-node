#!/bin/bash
sed "s/latest/$1/g" deployment.yaml > scheduler-service.yaml
sed "s/latest/$1/g" dev-deployment.yaml > dev-scheduler-service.yaml
sed "s/latest/$1/g" sb-deployment.yaml > sb-scheduler-service.yaml
sed "s/latest/$1/g" qa-deployment.yaml > qa-scheduler-service.yaml