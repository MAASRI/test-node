const { body } = require('express-validator');

// do validation(.isInt()) and Sanitization(.toInt())
module.exports = {
    createApiValidation: () => {
        return [
            body([
                "scheduleName",
                "requestingServiceName",
                "contentType",
            ], "Pass a valid value").notEmpty(),
            body(["second", "minute", "hour"]).isInt().toInt().optional(),
            body(["requestType"], 'valid values should be in ("get", "head", "post", "put", "delete", "connect", "options", "trace", "patch")').toLowerCase().isIn([
                "get", "head", "post", "put", "delete", "connect", "options", "trace", "patch"
            ]).optional(),
            body(["requestURL"], "pass a valid url").isURL({require_tld: false}),
            body(["startDate"], "startDate should be in YYYY-MM-DD format")
                .isDate({format:"YYYY-MM-DD", strictMode: true}).optional(),
            body(["endDate"], "endDate should be in YYYY-MM-DD format")
                .isDate({format:"YYYY-MM-DD", strictMode: true}).optional(),
            body(["recurrence"]).isBoolean().optional(),
            body(["intervalType"]).notEmpty().optional(),
            body(["interval"]).isInt().toInt().optional(),
            body([
                "lastTriggeredTime", "createdBy",
                "updatedBy", "createdTime", "updatedTime", "jonId"
            ], "This property will be populated by server, so don't send it")
                .not().exists(),
            body(["positionOfWeek"]).customSanitizer((value) => {
                // convert value to string if its type is not string
                return typeof value === "string" ? value : value.toFixed();
              }).isIn(["1", "2", "3", "4", "5", "last"]).withMessage(
                `positionOfWeek should be in ${["1", "2", "3", "4", "5", "last"]}`
            ).optional()
        ];
    },
    updateApiValidation: () => {
        return [
            body([
                "scheduleId",
                "requestingServiceName"
            ], "Pass a valid value").notEmpty(),
            body(['status'], "Pass a valid status in ('active' or 'inActvie')")
                .isIn(['active', 'inActive']).optional(),
            body(["requestURL"], "Pass a valid url").isURL({require_tld: false}).optional(),
            body([
                "scheduleName",
                "scheduleDescription",
                "contentType",
                "second",
                "minute",
                "hour",
                "recurrence",
            ]).notEmpty().optional(),
            body(["second", "minute", "hour"]).isInt().toInt().optional(),
            body(["requestType"], 'valid values should be in ("get", "head", "post", "put", "delete", "connect", "options", "trace", "patch")').toLowerCase().isIn([
                "get", "head", "post", "put", "delete", "connect", "options", "trace", "patch"
            ]).optional(),
            body(["intervalType"]).notEmpty().optional(),
            body(["interval"]).isInt().toInt().optional(),
            body([
                "lastTriggeredTime", "createdBy",
                "updatedBy", "createdTime", "updatedTime",
            ], "This property will be populated by server, so don't send it")
                .not().exists(),
            body(["positionOfWeek"]).customSanitizer((value) => {
                // convert value to string if its type is not string
                return typeof value === "string" ? value : value.toFixed();
                }).isIn(["1", "2", "3", "4", "5", "last"]).withMessage(
                `positionOfWeek should be in ${["1", "2", "3", "4", "5", "last"]}`
            ).optional()
        ];
    }
};
