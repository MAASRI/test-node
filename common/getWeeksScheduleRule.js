const nodeSchedule = require("node-schedule");
const _ = require("lodash");

exports.getWeeksScheduleRule = async ({
    interval, second, minute, hour, weekDays, positionOfWeek,
    startDate, endDate
}) => {
    // If interval is greater than 2 then return to client with message as `not supported as of now`
    let intervalLimitForWeeks = 2;
    if (interval > intervalLimitForWeeks) {
        throw new Error(`Interval greater than ${intervalLimitForWeeks} is not supported for weeks, pass either ${intervalLimitForWeeks} or less than ${intervalLimitForWeeks}`);
    }
    const weekToNumberMapper = {
        "0": {weekName: ["sun", "sunday"]},
        "1": {weekName: ["mon", "monday"]},
        "2": {weekName: ["tue", "tueday"]},
        "3": {weekName: ["wed", "wednesday"]},
        "4": {weekName: ["thu", "thursday"]},
        "5": {weekName: ["fri", "friday"]},
        "6": {weekName: ["sat", "saturday"]},
    };

    const rule = new nodeSchedule.RecurrenceRule();
    // @ts-ignore
    rule.second = second;
    // @ts-ignore
    rule.minute = minute;
    // @ts-ignore
    rule.hour = hour;

    // @ts-ignore
    let dayOfWeek = [];

    // Get the rule.dayOfWeek from the given weekdays value
    weekDays.forEach((weekDay) => {
        _.forEach(weekToNumberMapper, (weekNames, key) => {
            if (weekNames.weekName.indexOf(weekDay.toLowerCase()) > -1) {
                // @ts-ignore
                dayOfWeek.push(parseInt(key));
            }
        });
    });

    if (dayOfWeek.length > 0) {
        rule.dayOfWeek = dayOfWeek;
    }

    // If positionOfWeek exists then add corresponding 7 week dates to rule.date
    if (positionOfWeek) {
        if (positionOfWeek === "1") {
            rule.date = [new nodeSchedule.Range(1, 7, 1)];
        } else if (positionOfWeek === "2") {
            rule.date = [new nodeSchedule.Range(8, 14, 1)];
        } else if (positionOfWeek === "3") {
            rule.date = [new nodeSchedule.Range(15, 21, 1)];
        } else if (positionOfWeek === "4") {
            rule.date = [new nodeSchedule.Range(22, 28, 1)];
        } else if (positionOfWeek === "5") {
            rule.date = [29, 30, 31];
        } else if (positionOfWeek === "last") {
            if (weekDays.length === 0) {
                rule.date = [startDate.endOf("month").day];
            } else {
                rule.date = [
                    new nodeSchedule.Range(
                        startDate.endOf("month").day - 6,
                        startDate.endOf('month'),
                        1
                    )
                ];
            }
        }
    }

    const scheduleRule = {
        start: new Date(startDate.toFormat('yyyy-MM-dd')),
        rule
    };

    if(endDate) {
        scheduleRule.end = new Date(endDate);
    }
    return scheduleRule;
};
