const {name: appName} = require("../package.json");

// @ts-ignore
module.exports = require("rr-js/lib/nodeBunyan")({
    name: appName,
    logLevel: {
        stdout: process.env.LOG_LEVEL
    }
});
