const {DateTime} = require("luxon");
const nodeSchedule = require("node-schedule");
const { getWeeksScheduleRule } = require("./getWeeksScheduleRule");

module.exports.getCronRule = async ({
    second = 0,
    minute = 0,
    hour = 0,
    recurrence = true,
    startDate,
    endDate,
    intervalType,
    interval = 1,
    onServerRestart,
    weekDays = ["Mon"],
    positionOfWeek
}) => {

    // check for startDate, if not there then default to today(today at server timezone)
    if (startDate) {
        // check if startDate is in yyyy-MM-dd format, if yes then use fromISO to read
        // else use fromJSDate to read startDate which may be coming from db which is JS date.
        if (/^\d{4}\-(0?[1-9]|1[012])\-(0?[1-9]|[12][0-9]|3[01])$/.test(startDate)) {
            startDate = DateTime.fromISO(startDate);
        } else {
            // this line will execute if call is coming from rescheduleAllActiveJobs function.
            startDate = DateTime.fromJSDate(startDate);
        }
        if (!startDate.isValid) {
            throw new Error("Pass a valid startDate");
        }
    } else {
        startDate = DateTime.local();
    }

    if (endDate) {
        if (/^\d{4}\-(0?[1-9]|1[012])\-(0?[1-9]|[12][0-9]|3[01])$/.test(endDate)) {
            endDate = DateTime.fromISO(endDate);
        } else {
            endDate = DateTime.fromJSDate(endDate);
        }
    }

    // if recurrence === false
    if (recurrence === false) {

        // @ts-ignore
        let scheduleDate = startDate.set({second, minute, hour});

        // @ts-ignore
        if (scheduleDate < DateTime.local()) {
            throw new Error("startDate is in past, so can't schedule a job");
        }

        if (endDate && endDate < scheduleDate) {
            throw new Error("endDate can not be less than the scheduleDate(not startDate)");
        }

        // Convert to JS date as scheduleJob will accept js date only
        // @ts-ignore
        return new Date(scheduleDate);
    } else if (recurrence === true) {

        // Recurrence rule property values
        // second (0-59) // default 0
        // minute (0-59)
        // hour (0-23)
        // date (1-31)
        // month (0-11)
        // year
        // dayOfWeek (0-6) Starting with Sunday

        if (intervalType === "time") {

            let scheduleDate;
            if (onServerRestart) {
                scheduleDate = startDate.plus({
                    seconds: second, minutes: minute, hours: hour
                });

                do {
                    scheduleDate = scheduleDate.plus({
                        seconds: second, minutes: minute, hours: hour
                    });
                } while (scheduleDate < DateTime.utc());

            } else {
                if (startDate <= DateTime.local()) {
                    // add 5 seconds to make sure job is scheduled in future from now
                    scheduleDate = DateTime.local().plus({seconds: 5});
                } else {
                    scheduleDate = startDate;
                }
            }

            // @ts-ignore
            if (scheduleDate < DateTime.local()) {
                throw new Error("startDate is in past, so can't schedule a job");
            }

            if (endDate && endDate < scheduleDate) {
                throw new Error("endDate can not be less than the scheduleDate");
            }

            return new Date(scheduleDate);

        } else if (intervalType === "days") {

            let scheduleDate;
            if (onServerRestart) {
                scheduleDate = startDate.plus({days: interval});
            } else {
                scheduleDate = startDate.set({second, minute, hour});
            }

            if (endDate && endDate < scheduleDate) {
                throw new Error("endDate can not be less than the scheduleDate(not startDate)");
            }

            return new Date(scheduleDate);
        } else if (intervalType === "weeks") {
            return getWeeksScheduleRule({
                interval, second, minute, hour, weekDays, positionOfWeek,
                startDate, endDate
            });
        } else if (intervalType === "months") {
            const rule = new nodeSchedule.RecurrenceRule();

            // @ts-ignore
            rule.second = second;
            // @ts-ignore
            rule.minute = minute;
            // @ts-ignore
            rule.hour = hour;
            rule.date = startDate.day;
            rule.month = [new nodeSchedule.Range(0, 11, interval)];
            const scheduleRule = {
                start: new Date(startDate.toFormat('yyyy-MM-dd')),
                rule
            };

            if(endDate) {
                scheduleRule.end = new Date(endDate);
            }
            return scheduleRule;
        } else {
            // It repeats daily at the given second, minute, hour
            // this is rule based scheduling
            const rule = new nodeSchedule.RecurrenceRule();
            // @ts-ignore
            rule.second = second;
            // @ts-ignore
            rule.minute = minute;
            // @ts-ignore
            rule.hour = hour;

            const scheduleRule = {
                start: new Date(startDate.toFormat('yyyy-MM-dd')),
                rule
            };

            if(endDate) {
                scheduleRule.end = new Date(endDate);
            }
            return scheduleRule;
        }
    }
};
