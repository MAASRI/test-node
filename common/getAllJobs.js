const nodeSchedule = require('node-schedule');
const {DateTime} = require("luxon");

exports.getAllJobs = async () => {
    /**
    {
        'b1fb92a0-dad9-47c0-8e25-e0a830481bfd': Job {
            job: [AsyncFunction (anonymous)],
            callback: false,
            name: 'b1fb92a0-dad9-47c0-8e25-e0a830481bfd',
            trackInvocation: [Function (anonymous)],
            stopTrackingInvocation: [Function (anonymous)],
            triggeredJobs: [Function (anonymous)],
            setTriggeredJobs: [Function (anonymous)],
            cancel: [Function (anonymous)],
            cancelNext: [Function (anonymous)],
            reschedule: [Function (anonymous)],
            nextInvocation: [Function (anonymous)],
            pendingInvocations: [Function (anonymous)],
            _events: [Object: null prototype] {
            run: [AsyncFunction (anonymous)],
            scheduled: [Function (anonymous)],
            canceled: [Function (anonymous)]
            },
            _eventsCount: 3
        }
    }
    */
    let jobDetails = [];
    let jobs = nodeSchedule.scheduledJobs;
    Object.keys(jobs).forEach((jobId) => {
        let job = jobs[jobId];
        let info = {
            jobId: job.name
        };
        if (job.nextInvocation()) {
            info.nextInvocation = job.nextInvocation()._date.toISOString();
        } else {
            info.nextInvocation = null;
        }

        info.pendingInvocations = job.pendingInvocations().length;
        info.triggeredJobs = job.triggeredJobs();
        jobDetails.push(info);
    });
    return jobDetails;
};
