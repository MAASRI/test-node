const swaggerJSDoc = require('swagger-jsdoc');
const swaggerDocument = require('../swaggerConfig.json');
// const swaggerParser = require('swagger-parser');
const writeData  = require("write-data");

console.log("Generating swagger.yaml file....");
writeData.sync(`${__dirname}/../contracts/swagger.yml`, swaggerJSDoc(swaggerDocument), {});
console.log("Done");
