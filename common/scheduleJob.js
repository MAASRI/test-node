const nodeSchedule = require('node-schedule');
const logger = require("./logger");
const { callRemoteAPI } = require("./callRemoteAPI");
const db = require('./db');
let { DateTime } = require("luxon");
const { scheduleUpdate } = require("./updateOperations");
const { processAuditUpdate } = require("../common/updateOperations");
const { v4: uuidv4 } = require('uuid');
const { getWeeksScheduleRule } = require('./getWeeksScheduleRule');


exports.scheduleJob = async ({
    clientId,
    jobId,
    requestType: method = 'GET',
    contentType = 'application/json',
    requestURL: url,
    requestBody: params,
    recurrence,
    scheduleRule,
    second = 0, minute = 0, hour = 0, endDate,
    intervalType,
    interval,
    weekDays,
    rrId,
    solutionName,
    processId = uuidv4(),
    positionOfWeek,
    startDate
}) => {

    const clientDBConnection = await db.useDb(clientId);
    const scheduleModel = await clientDBConnection.model('schedule');
    /**
     * Schedule job in machine(on the server where this code is running)
     */
    // @ts-ignore
    let job = new nodeSchedule.Job(jobId, async (fireDate) => {

        let updateStatus = ({ jobId }) => {
            scheduleModel.findOneAndUpdate({
                jobId
            }, {
                status: "inActive"
            }, {
                new: true
            }, async (err, doc, res) => {
                if (err) {
                    logger.info(rrId, solutionName,
                        'Updating status to inActive failed after cancelling the job',
                        { err: err },
                    );
                }
            });
        };

        // update schedule with last fireDate(last scheduledDate)
        scheduleUpdate({ clientId, jobId, fireDate });

        if (recurrence === false) {
            if (job.cancel()) {
                logger.info(rrId, solutionName, "Job has been canceled", {
                    processId, status: "stopped", remark: "process has be cancelled"
                });
                updateStatus({ jobId });
                // update processAudit collection
                // @ts-ignore
                processAuditUpdate({ clientId, rrId, processId, status: "stopped", remark: "process has be cancelled" });
            }
        } else if (recurrence === true) {
            if (intervalType === "time") {
                // @ts-ignore
                let nextScheduleDate = DateTime.fromJSDate(fireDate);
                nextScheduleDate = nextScheduleDate.plus({
                    seconds: second, minutes: minute, hours: hour
                });
                if (nextScheduleDate > DateTime.fromJSDate(endDate)) {
                    if (job.cancel()) {
                        logger.info(rrId, solutionName, "Job has been canceled", {
                            processId, status: "stopped", remark: "process has been cancelled"
                        });
                        updateStatus({ jobId });
                        // update processAudit collection
                        // @ts-ignore
                        processAuditUpdate({ clientId, rrId, processId, status: "stopped", remark: "process has been cancelled" });
                    }
                } else {
                    // @ts-ignore
                    job.reschedule(new Date(nextScheduleDate));
                }
            } else if (intervalType === "days") {
                // @ts-ignore
                let nextScheduleDate = DateTime.fromJSDate(fireDate);
                nextScheduleDate = nextScheduleDate.plus({
                    days: interval
                });
                if (nextScheduleDate > DateTime.fromJSDate(endDate)) {
                    if (job.cancel()) {
                        logger.info(rrId, solutionName, "Job has been canceled", {
                            processId, status: "stopped", remark: "process has been cancelled"
                        });
                        updateStatus({ jobId });
                        // update processAudit collection
                        // @ts-ignore
                        processAuditUpdate({ clientId, rrId, processId, status: "stopped", remark: "process has been cancelled" });
                    }
                } else {
                    // @ts-ignore
                    job.reschedule(new Date(nextScheduleDate));
                }
            } else if (intervalType === "weeks") {
                if (interval === 2) {
                    if (weekDays.length === job.triggeredJobs()) {
                        // cancel next n(=== weekdays.length) invocations of the the job to skip the one week
                        Array.from({ length: weekDays.length }).forEach(i => job.cancelNext());
                        job.setTriggeredJobs(0);
                    }
                } else if (positionOfWeek === "last") {
                    if (job.pendingInvocations().length === 0) {
                        // adding 10 days to just go to next month from the last firedate.
                        let startDate = DateTime.fromJSDate(fireDate).plus({ days: 10 });
                        let scheduleRule = await getWeeksScheduleRule({
                            interval, second, minute, hour, weekDays, positionOfWeek,
                            startDate,
                            endDate: DateTime.fromJSDate(endDate)
                        });
                        // @ts-ignore
                        job.reschedule(scheduleRule);
                    }
                }
            } else if (job.pendingInvocations().length === 0) {
                if (job.cancel()) {
                    logger.info(rrId, solutionName, "Job has been canceled", {
                        processId, status: "stopped", remark: "process has been cancelled"
                    });
                    updateStatus({ jobId });
                    // update processAudit collection
                    // @ts-ignore
                    processAuditUpdate({ clientId, rrId, processId, status: "stopped", remark: "process has been cancelled" });
                }
            }
        }

        callRemoteAPI({ clientId, solutionName, processId, method, contentType, url, params });

    });

    logger.info(rrId, solutionName, `scheduleRule is ${JSON.stringify(scheduleRule)}`);
    if (job.schedule(scheduleRule)) {
        // update processAudit collection
        processAuditUpdate({
            clientId, rrId, processId, processTime: new Date(),
            status: "running", remark: ""
        });
        return job;
    } else {
        if (job.cancel()) {
            // update processAudit collection
            processAuditUpdate({
                clientId, rrId, processId, processTime: new Date(),
                status: "failed", remark: "Could not schedule the job"
            });
            return null;
        } else {
            logger.info(rrId, solutionName, `Cancellation of job failed, jobId is ${job.name}`, {
                processId, status: "stopped"
            });
            // update processAudit collection
            processAuditUpdate({
                clientId, rrId, processId, processTime: new Date(),
                status: "failed", remark: `Cancellation of job failed, jobId is ${job.name}`
            });
            return null;
        }
    }
};
