const logger = require("./logger");
const { getMongooseModelRef } = require("./getMongooseModelRef");
const app = require("../app");
const { name: appName } = require("../package.json");

exports.scheduleUpdate = async ({clientId, jobId, fireDate}) => {

    const scheduleModel = await getMongooseModelRef({
        clientId, modelName: "schedule"
    });
    scheduleModel.findOneAndUpdate({
        jobId
    }, {
        lastTriggeredTime: fireDate,
    }, {
        new: true
    }, async (err, doc, res) => {
        if (err) {
            logger.info(app.get("rrId"), appName,
                'Updating status to inActive failed after cancelling the job',
                {err: err},
            );
        }
    });
};

exports.requestAuditUpdate = async ({clientId, rrId, solutionName, requestBody, processTime, status, remark}) => {
    const requestAuditModel = await getMongooseModelRef({
        clientId, modelName: "requestAudit"
    });

    await requestAuditModel.findOneAndUpdate({
        rrId
    }, {
        solutionName, requestBody, processTime, status, remark
    }, {
        upsert: true // Make this update into an upsert
    });
};

exports.processAuditUpdate = async ({clientId, rrId, processId, processTime, status, remark}) => {
    const processAuditModel = await getMongooseModelRef({
        clientId, modelName: "processAudit"
    });

    await processAuditModel.findOneAndUpdate({
        rrId,
        processId
    }, {
        processTime, status, remark
    }, {
        new: true,
        upsert: true // Make this update into an upsert
    });
};

exports.transactionAuditUpdate = async ({
    clientId, rrId, processId, transactionId, processTime, status, remark
}) => {
    const transactionAuditModel = await getMongooseModelRef({
        clientId, modelName: "transactionAudit"
    });

    await transactionAuditModel.findOneAndUpdate({
        rrId,
        processId,
        transactionId
    }, {
        processTime, status, remark
    }, {
        new: true,
        upsert: true // Make this update into an upsert
    });
};
