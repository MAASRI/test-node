const { scheduleJob } = require('./scheduleJob');
const db = require('./db.js');
const logger = require("./logger");
const { getCronRule } = require('./getCronRule');
const app = require("../app");
const { name: appName } = require("../package.json");

exports.rescheduleAllActiveJobs = async ({ clientId }) => {
    const clientDBConnection = await db.useDb(clientId);
    const scheduleModel = await clientDBConnection.model('schedule');
    // Get the active jobs from mongodb
    let allActiveJobs = await scheduleModel
        .find({ clientId, status: 'active' })
        .exec();
    let jobsStatus = {
        totalActiveJobs: allActiveJobs.length,
        failed: 0,
        success: 0,
        data: [],
    };
    for (const schedule of allActiveJobs) {
        let eachJobStatus = {
            scheduleId: schedule.scheduleId
        };
        let job;
        try {
            let scheduleRule = await getCronRule({
                ...schedule._doc,
                startDate: schedule.lastTriggeredTime,
                onServerRestart: true
            });
            job = await scheduleJob({ ...schedule._doc, scheduleRule, rrId: app.get("rrId"), solutionName: appName });
        } catch (e) {
            logger.info(app.get("rrId"), appName, "Got issue while rescheduling the job on server start", { err: e });
        }

        if (!job) {
            jobsStatus.failed += 1;
            eachJobStatus.success = false;
        } else {
            jobsStatus.success += 1;
            eachJobStatus.success = true;
        }
        // @ts-ignore
        jobsStatus.data.push(eachJobStatus);
    }

    return jobsStatus;
};
