const mongoose = require('mongoose');
const { ips } = require('../Environment/ipConfig');

const clientOption = {
    autoIndex: false,
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify: false,
    authSource: "admin"
};

// @ts-ignore
const db = mongoose.createConnection(ips.db, clientOption);

db.on(
    'error',
    console.error.bind(console, 'MongoDB Connection Error>> : '),
);
db.once('open', function () {
    console.log('client MongoDB Connection ok!');
});

module.exports = db;
