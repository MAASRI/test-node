const axios = require('axios').default;
const qs = require('qs');
const { transactionAuditUpdate } = require("./updateOperations");
const { v4: uuidv4 } = require('uuid');
const logger = require("./logger");


exports.callRemoteAPI = async ({ clientId, solutionName, processId,
    transactionId = uuidv4(),
    method, contentType, url, params
}) => {
    let processTime = new Date();
    // Generate new rrId for each new request that we are making here
    const rrId = uuidv4();
    let options = {
        method,
        headers: { 'content-type': contentType, rrId, solutionName },
        url,
    };
    if (['POST', 'post', 'PUT', 'put', 'DELETE', 'delete'].includes(method)) {
        options.data = qs.parse(params);
    }

    if (['GET', 'get'].includes(method)) {
        options.params = qs.parse(params);
    }

    // @ts-ignore
    axios(options)
        .then((response) => {
            if (response.status !== 200) {
                logger.info(
                    rrId, solutionName,
                    JSON.stringify(response.data), {
                    clientId, processId, transactionId, processTime, status: "failed", remark: response.status
                }
                );
                transactionAuditUpdate({ clientId, rrId, processId, transactionId, processTime, status: "failed", remark: response.status });
            } else {
                logger.info(
                    rrId, solutionName,
                    JSON.stringify(response.data), {
                    clientId, processId, transactionId, processTime, status: "success", remark: JSON.stringify(response.data)
                }
                );
                transactionAuditUpdate({ clientId, rrId, processId, transactionId, processTime, status: "success", remark: JSON.stringify(response.data) });
            }
        })
        .catch((error) => {
            logger.info(
                rrId, solutionName,
                JSON.stringify(error.message), {
                clientId, processId, transactionId, processTime, status: "failed", remark: JSON.stringify(error.message)
            }
            );
            transactionAuditUpdate({ clientId, rrId, processId, transactionId, processTime, status: "failed", remark: JSON.stringify(error.message) });
            // if (error.response) {
            //     // The request was made and the server responded with a status code
            //     // that falls out of the range of 2xx
            //     // logger.info(error.response.data);
            //     // logger.info(error.response.status);
            //     // logger.info(error.response.headers);
            //     transactionAuditUpdate({clientId, rrId, processId, transactionId, processTime, status: "failed", remark: JSON.stringify(error.response.data)});
            // } else if (error.request) {
            //     // The request was made but no response was received
            //     // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
            //     // http.ClientRequest in node.js
            //     // logger.info(error.request);
            //     transactionAuditUpdate({clientId, rrId, processId, transactionId, processTime, status: "failed", remark: JSON.stringify(error.request)});
            // } else {
            //     // Something happened in setting up the request that triggered an Error
            //     // logger.info('Error', error.message);
            //     transactionAuditUpdate({clientId, rrId, processId, transactionId, processTime, status: "failed", remark: JSON.stringify(error.message)});
            // }
            // logger.info(error.config);
        });
};
