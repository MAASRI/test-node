const nodeSchedule = require('node-schedule');

exports.cancelJob = async ({ jobId }) => {
    const jobRef = nodeSchedule.scheduledJobs[jobId];

    if (!jobRef) return null;

    // cancel the job using cancel() method
    if (jobRef.cancel()) {
        return { message: 'Job was deleted successfully', deleted: true };
    } else {
        return { message: 'Job deletion failed', deleted: false };
    }
};
