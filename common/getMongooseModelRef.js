const db = require('./db');

exports.getMongooseModelRef = async ({ clientId, modelName }) => {
    const clientDBConnection = await db.useDb(clientId);
    return await clientDBConnection.model(modelName);
};
