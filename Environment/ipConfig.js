exports.ips = {
    db: process.env.mongoServer,
    userManagment: "http://user-service",
    ner: "http://ner-service",
    scheduler: "http://scheduler-service",
    emailService: "http://email-service",
    dpaService: "http://cm-dpa-service",
    kansaService: "http://kansa-data-service",
    kansaServiceCreateDev: "https://api.df-dev.net/audit",
    docx:"http://docx-service",
    blackGateService: "http://black-gate"
};
