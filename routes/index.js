const express = require('express');
const router = express.Router();
const swaggerJSDoc = require('swagger-jsdoc');
const swaggerUi = require('swagger-ui-express');
const swaggerDocument = require('../swaggerConfig.json');
// const swaggerParser = require('swagger-parser');
const { getAllJobs } = require('../common/getAllJobs');

// swagger api docs
const swaggerSpec = swaggerJSDoc(swaggerDocument);

/**
 * @swagger
 * /:
 *  get:
 *    summary: "Get welcome message"
 *    description: Welcome to Scheduler Service
 *    responses:
 *      200:
 *        description: Welcome to Scheduler Service
 */
router.get('/', async (req, res) => {
    res.json({ message: 'Welcome to Scheduler Service' });
});

router.use(
    '/api-docs',
    swaggerUi.serve,
    swaggerUi.setup(swaggerSpec, { explorer: true }),
);

/**
 * @swagger
 * /1.0.0/swagger.json:
 *  get:
 *    summary: Get swagger spec
 *    description: Returns swagger api spec in json format
 *    responses:
 *      200:
 *        description: Returns swagger api spec in json format
 */
router.get('/1.0.0/swagger.json', async (req, res, next) => {
    // Validate the swagger spec
    try {
        // let api = await swaggerParser.validate(swaggerSpec);
        return res.json(swaggerSpec);
    } catch (err) {
        console.error(err);
        return next(err);
    }
});

/**
 * @swagger
 * /jobs:
 *  get:
 *      summary: Get all running jobs
 *      description: Returns a list of all running cron jobs
 *      responses:
 *          200:
 *              description: List of jobs
 */
router.get('/jobs', async (req, res, next) => {
    let result = await getAllJobs();
    return res.json(result);
});

module.exports = router;
