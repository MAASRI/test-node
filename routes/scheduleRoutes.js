const express = require('express');
const router = express.Router();
const scheduleController = require('../controllers/scheduleController.js');
const { createApiValidation, updateApiValidation } = require("../common/customValidation");
const { body } = require('express-validator');

/**
 * @swagger
 * components:
 *  schemas:
 *      Schedule:
 *          type: object
 *          required:
 *              - clientId
 *              - scheduleId
 *              - scheduleName
 *              - requestingServiceName
 *              - requestURL
 *              - requestBody
 *              - requestType
 *              - contentType
 *              - lastTriggeredTime
 *              - createdBy
 *              - updatedBy
 *              - createdTime
 *              - updatedTime
 *              - jobId
 *              - status
 *              - recurrence
 *              - weekDays
 *              - positionOfWeek
 *          properties:
 *              scheduleName:
 *                  type: string
 *              scheduleDescription:
 *                  type: string
 *              requestingServiceName:
 *                  type: string
 *              requestType:
 *                  type: string
 *              requestURL:
 *                  type: string
 *              contentType:
 *                  type: string
 *              requestBody:
 *                  anyOf:
 *                      - type: string
 *                      - type: object
 *              status:
 *                  type: string
 *              startDate:
 *                  type: string
 *              endDate:
 *                  type: string
 *              second:
 *                  type: integer
 *              minute:
 *                  type: integer
 *              hour:
 *                  type: integer
 *              recurrence:
 *                  type: boolean
 *              intervalType:
 *                  type: string
 *              interval:
 *                  type: integer
 *              weekDays:
 *                  type: array
 *                  items:
 *                      type: string
 *              positionOfWeek:
 *                  type: string
 *              clientId:
 *                  type: string
 *                  readOnly: true
 *              scheduleId:
 *                  type: string
 *                  readOnly: true
 *              lastTriggeredTime:
 *                  type: string
 *                  readOnly: true
 *              createdBy:
 *                  type: string
 *                  readOnly: true
 *              updatedBy:
 *                  type: string
 *                  readOnly: true
 *              createdTime:
 *                  type: string
 *                  readOnly: true
 *              updatedTime:
 *                  type: string
 *                  readOnly: true
 *              jobId:
 *                  type: string
 *                  readOnly: true
 *  requestBodies:
 *      createSchedule:
 *          description: create schedule
 *          content:
 *              application/json:
 *                  schema:
 *                      type: object
 *                      required:
 *                          - clientId
 *                          - scheduleName
 *                          - requestingServiceName
 *                          - requestURL
 *                          - requestBody
 *                          - requestType
 *                          - contentType
 *                      properties:
 *                          clientId:
 *                              type: string
 *                          scheduleName:
 *                              type: string
 *                          scheduleDescription:
 *                              type: string
 *                          requestingServiceName:
 *                              type: string
 *                          requestType:
 *                              type: string
 *                          requestURL:
 *                              type: string
 *                          contentType:
 *                              type: string
 *                          requestBody:
 *                              anyOf:
 *                                  - type: string
 *                                  - type: object
 *      updateSchedule:
 *          description: update schedule
 *          content:
 *              application/json:
 *                  schema:
 *                      type: object
 *                      required:
 *                          - clientId
 *                          - scheduleId
 *                          - requestingServiceName
 *                      properties:
 *                          scheduleId:
 *                              type: string
 *                          clientId:
 *                              type: string
 *                          requestingServiceName:
 *                              type: string
 *      listSchedules:
 *          description: list all active and inactive schedules
 *          content:
 *              application/json:
 *                  schema:
 *                      type: object
 *                      required:
 *                          - clientId
 *                      properties:
 *                          clientId:
 *                              type: string
 *                          status:
 *                              type: string
 *      deleteSchedules:
 *          description: delete schedules
 *          content:
 *              application/json:
 *                  schema:
 *                      type: object
 *                      required:
 *                          - clientId
 *                          - scheduleIds
 *                      properties:
 *                          clientId:
 *                              type: string
 *                          scheduleIds:
 *                              type: array
 *                              items:
 *                                  type: string
 *                              example: ["uuidv4_1", "uuidv4_v2"]
 */

/**
 * @swagger
 * /schedules/list:
 *  post:
 *      summary: Get list of schedules(both active and inActive)
 *      description: Get all active and inactive schedules
 *      requestBody:
 *          $ref: '#/components/requestBodies/listSchedules'
 *      responses:
 *          200:
 *              description: success
 */
router.post('/list', [
    body(["clientId"], "clientId field should not empty").notEmpty()
], scheduleController.list);

/**
 * @swagger
 * /schedules/create:
 *
 *  post:
 *      summary: Create a schedule(job)
 *      description: create a schedule
 *      requestBody:
 *          $ref: '#/components/requestBodies/createSchedule'
 *      responses:
 *          201:
 *              description: Successfully created a schedule
 */
// @ts-ignore
router.post('/create', [createApiValidation()], scheduleController.create);

/**
 * @swagger
 * /schedules/update:
 *  put:
 *      summary: Update the schedule
 *      description: Update the schedule(delete the existing job and create new job)
 *      requestBody:
 *          $ref: '#/components/requestBodies/updateSchedule'
 *      responses:
 *          201:
 *              description: Successfully updated the schedule
 */
router.put('/update', updateApiValidation(), scheduleController.update);

/**
 * @swagger
 * /schedules/delete:
 *  post:
 *      summary: Delete schedules
 *      description: delete the given schedule(s)/job(s) using jobId
 *      requestBody:
 *          $ref: "#/components/requestBodies/deleteSchedules"
 *      responses:
 *          202:
 *              description: Successfully deleted the schedule(s)
 */
router.post('/delete', scheduleController.remove);

module.exports = router;
