const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const processAuditSchema = new Schema({
    rrId: {
        type: String,
        require: true
    },
    processId: {
        type: String,
        require: true
    },
    processTime: {
        type: Date,
        require: true
    },
    status: {
        type: String
    },
    remark: {
        type: String
    }
});

module.exports = mongoose.model('processAudit', processAuditSchema);
