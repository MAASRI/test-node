const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const scheduleSchema = new Schema({
    clientId: {
        type: String,
        require: true,
    },
    scheduleId: {
        type: String,
        require: true,
    },
    scheduleName: {
        type: String,
        require: true,
    },
    scheduleDescription: {
        type: String,
    },
    requestingServiceName: {
        type: String,
        require: true,
    },
    requestType: {
        type: String,
        require: true,
    },
    requestURL: {
        type: String,
        require: true,
    },
    contentType: {
        type: String,
        require: true,
    },
    requestBody: {
        type: Schema.Types.Mixed,
        require: true,
    },
    lastTriggeredTime: {
        type: Date,
    },
    createdBy: {
        type: String,
        require: true,
    },
    updatedBy: {
        type: String,
    },
    createdTime: {
        type: Date,
        require: true,
    },
    updatedTime: {
        type: Date,
    },
    jobId: {
        type: String,
        require: true,
    },
    status: {
        type: String,
        enum: ["active", "inActive", "deleted"],
        require: true,
    },
    startDate: {
        type: Date,
    },
    endDate: {
        type: Date,
    },
    hour: {
        type: Number,
    },
    minute: {
        type: Number,
    },
    second: {
        type: Number,
    },
    recurrence: {
        type: Boolean,
    },
    intervalType: {
        type: String
    },
    interval: {
        type: Number
    },
    weekDays: {
        type: Array
    },
    positionOfWeek: {
        type: String
    }
});

module.exports = mongoose.model('schedule', scheduleSchema);
