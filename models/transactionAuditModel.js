const mongoose = require("mongoose");

const transactionAuditSchema = new mongoose.Schema({
    rrId: {
        type: String,
        require: true
    },
    processId: {
        type: String,
        require: true
    },
    transactionId: {
        type: String,
        require: true
    },
    processTime: {
        type: Date,
        require: true
    },
    status: {
        type: String
    },
    remark: {
        type: String
    }
});

module.exports = mongoose.model("transactionAudit", transactionAuditSchema);
