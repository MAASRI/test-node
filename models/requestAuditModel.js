const mongoose = require("mongoose");

const requestAuditSchema = new mongoose.Schema({
    rrId: {
        type: String,
        require: true
    },
    solutionName: {
        type: String,
        require: true
    },
    requestBody: {
        type: JSON,
        require: true
    },
    requestTime: {
        type: Date,
        require: true
    },
    status: {
        type: String,
    },
    remark: {
        type: String
    }
});

module.exports = mongoose.model("requestAudit", requestAuditSchema);
