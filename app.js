const createError = require('http-errors');
const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const logger = require("./common/logger");
const fs = require('fs');
const indexRouter = require('./routes/index');
const scheduleRoutes = require('./routes/scheduleRoutes');
const helmet = require('helmet');
const cors = require('cors');
const db = require('./common/db.js');
const _ = require('lodash');
const asyncHandler = require('express-async-handler');
const { body, validationResult } = require('express-validator');
const { v4: uuidv4 } = require('uuid');
const { name: appName } = require("./package.json");
const morgan = require('morgan');

// Load all models from ./models directory
fs.readdirSync(__dirname + '/models').forEach(function (file) {
    if (~file.indexOf('.js')) require(__dirname + '/models/' + file);
});

process.on('unhandledRejection', (error, promise) => {
    logger.info(app.get('rrId'), appName, `We forgot to handle a promise rejection here: ${promise}`, { err: error });
});

const app = express();
app.set("rrId", uuidv4());

app.use(morgan('combined'));

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(helmet());
//@ts-ignore
app.use(cors());
// Accept custom headers
// @ts-ignore
app.options('*', cors());

// This route should be before database initialization, as these doesn't require client id in body
app.use('/', indexRouter);

// attach mongodb instance to req object
// TODO: check for clientId in user_session custom header
app.use(
    [
        body(["clientId"], "Pass a valid clientId").notEmpty(),
    ],
    asyncHandler(async (req, res, next) => {
        /**
       * Validation
       */
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            return res.status(400).json({ errors: errors.array() });
        }

        const clientDBConnection = await db.useDb(req.body.clientId);
        // @ts-ignore
        req.db = clientDBConnection;
        next();
    }),
);

// process rrId and solutionName
app.use(asyncHandler(async (req, res, next) => {

    // get rrId from headers if not then generate one(uuid)   
    if (_.has(req, "headers.rrid") || _.has(req, "headers.rrId")) {
        // @ts-ignore
        if (req.headers.rrid) {
            req.rrId = req.headers.rrid;
        } else if (req.headers.rrId) {
            req.rrId = req.headers.rrId;
        }
    } else if (_.has(req, "body.rrId")) {
        // @ts-ignore
        req.rrId = req.body.rrId;
    } else {
        // @ts-ignore
        req.rrId = uuidv4();
    }

    // get solutionName from body if not then use app name(from (package.json)["name"])
    if (_.has(req, "body.solutionName")) {
        // @ts-ignore
        req.solutionName = req.body.solutionName;
    } else {
        // @ts-ignore
        req.solutionName = appName;
    }
    next();
}));

app.use('/schedules', scheduleRoutes);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
    next(createError(404));
});

// error handler
app.use(function (err, req, res, next) {
    if (res.headersSent) {
        return next(err);
    }

    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};

    // render the error page
    res.status(err.status || 500);
    // console.log("in error handler");
    // console.log(err)
    res.json({ message: err.message, stack: err.stack });
});

module.exports = app;
