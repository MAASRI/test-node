const nodeSchedule = require("node-schedule");
const sinon  = require('sinon');
const clock = sinon.useFakeTimers(Date.now());
const luxon = require("luxon");
const { getCronRule } = require("../common/getCronRule");


let interval = 2;
let weekDays = [
    1, 2
];
const scheduleHandler = async (x, fireDate,) => {
    const nowIs = new Date();
    const latency = nowIs.getTime() - new Date(fireDate).getTime();
    console.log("Now is         ", nowIs);
    console.log("Latency is ", latency);
    console.log(x);
    console.log("triggered jobs are", job.triggeredJobs());

    if (interval === 2) {
        if (weekDays.length === job.triggeredJobs()) {
            // cancel next 2 invocations of the the job
            Array.from({length: weekDays.length}).forEach(i => job.cancelNext());
            job.setTriggeredJobs(0);
        }
    }
    // job.cancelNext();
    // job.cancelNext();
};

const job = new nodeSchedule.Job("test", scheduleHandler.bind(null, {some: "dynamic", data: "here"}));

console.log("nextInvocation ", job.nextInvocation());

job.on("run", () => {
    console.log("in run event subscriber");
});
job.on("canceled", (data, date) => {
    console.log("in cancel event subscriber");
    console.log(data, date);
});
job.on("scheduled", (nextInvocation) => {
    console.log(new Date(), "scheduled at   ", nextInvocation);
});

let startTime = new Date(Date.now() + 120000);
let endTime = new Date(startTime.getTime() + 180000);
const rule = new nodeSchedule.RecurrenceRule();
// const rule = new Date(Date.now() + 20000);
rule.hour = 0;
rule.minute = 0;
// rule.second = 0;
rule.dayOfWeek = [1];

let scheduleRule;
(async () => {
    // @ts-ignore
    // scheduleRule = await getCronRule({
    //     startDate: "2021-01-01",
    //     endDate: "2021-02-28",
    //     intervalType: "weeks",
    //     interval: 2,
    //     weekDays: ["Mon"]
    // });

    scheduleRule = {start: new Date("2021-01-01"), rule};
    console.log("scheduleRule is ", scheduleRule);

    // const isJobScheduled = job.schedule({start: startTime, end: endTime, rule});
    // @ts-ignore
    const isJobScheduled = job.schedule(scheduleRule);

    if (!isJobScheduled) {
        console.log("Could not schedule the job");
    }

    setTimeout(() => {
        // job.reschedule("* * * * * *");
        // job.cancelNext();
        // job.cancelNext();
    }, 90000);

})();

clock.tick(5000000000);
