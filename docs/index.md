last, [], ["mon", ...]
first, [], ["mon", ...]

2020-11-30T12:03:00.000Z
const dt = luxon.DateTime.fromISO("2020-11-30T12:03:00.000Z");
dt.toJSDate()

DateTime.fromISO('2014-08-06T13:07:04.054').toFormat('yyyy LLL dd'); //=> '2014 Aug 06'

luxon.DateTime.local().zoneName;
'Asia/Calcutta'
> luxon.DateTime.fromISO("2020-12-16", {zone: "Asia/Calcutta"}).toString()
'2020-12-16T00:00:00.000+05:30'
> luxon.DateTime.local().toString()
'2020-12-16T17:53:53.257+05:30'

vscode REST examples

```http
POST /recover-password HTTP/1.1
Host: localhost:3000
Content-Type: text/plain

my@email.com
```

'@yearly': '0 0 1 1 *',
  '@monthly': '0 0 1 * *',
  '@weekly': '0 0 * * 0',
  '@daily': '0 0 * * *',
  '@hourly': '0 * * * *'

// const job = new schedule.Job(function() {
    //    test.ok(true);
    //   });
    //   job.schedule(new Date(Date.now() + 3000));


Remind at
Later Today 9 PM
{"IsReminderOn":true,"ReminderDateTime":{"DateTime":"2020-12-14T15:30:00.742Z","TimeZone":"UTC"}}

Due Date
{"DueDateTime":{"DateTime":"2020-12-25","TimeZone":"Asia/Calcutta"}}

Daily
{"DueDateTime":{"DateTime":"2020-12-25","TimeZone":"Asia/Calcutta"},"Recurrence":{"FirstDayOfWeek":"Sunday","RecurrenceType":"Daily"}}

WeekDays
{"DueDateTime":{"DateTime":"2020-12-25","TimeZone":"Asia/Calcutta"},"Recurrence":{"FirstDayOfWeek":"Sunday","RecurrenceType":"WeekDays"}}

Weekly
{"DueDateTime":{"DateTime":"2020-12-25","TimeZone":"Asia/Calcutta"},"Recurrence":{"FirstDayOfWeek":"Sunday","RecurrenceType":"Weekly"}}

Monthly
{"DueDateTime":{"DateTime":"2020-12-25","TimeZone":"Asia/Calcutta"},"Recurrence":{"FirstDayOfWeek":"Sunday","RecurrenceType":"Monthly"}}

Yearly
{"DueDateTime":{"DateTime":"2020-12-25","TimeZone":"Asia/Calcutta"},"Recurrence":{"FirstDayOfWeek":"Sunday","RecurrenceType":"Yearly"}}

Custom
=======
1.   {"DueDateTime":{"DateTime":"2020-12-25","TimeZone":"Asia/Calcutta"},"Recurrence":{"FirstDayOfWeek":"Sunday","RecurrenceType":"Custom","IntervalType":"Days","Interval":2}}

2.   {"DueDateTime":{"DateTime":"2020-12-14","TimeZone":"Asia/Calcutta"},"Recurrence":{"FirstDayOfWeek":"Sunday","RecurrenceType":"Custom","IntervalType":"Weeks","Interval":2,"WeekDays":["Monday","Tuesday","Wednesday"]}}


{
    "DueDateTime":{"DateTime":"2020-08-02","TimeZone":"Asia/Calcutta"},
    "Recurrence":{
        "FirstDayOfWeek":"Sunday",
        "RecurrenceType":"Custom",
        "IntervalType":"Weeks",
        "Interval":2,
        "WeekDays":["Tuesday"]
    }
}

{
  "DueDateTime": {
    "DateTime": "2020-08-04",
    "TimeZone": "Asia/Calcutta"
  },
  "Recurrence": {
    "FirstDayOfWeek": "Sunday",
    "RecurrenceType": "Custom",
    "IntervalType": "Days",
    "Interval": 2
  }
}

{
  "DueDateTime": {
    "DateTime": "2020-12-15",
    "TimeZone": "Asia/Calcutta"
  },
  "Recurrence": {
    "FirstDayOfWeek": "Sunday",
    "RecurrenceType": "Custom",
    "IntervalType": "Months",
    "Interval": 2
  }
}

intervalType = days, weeks, months, years
interval = 1,2

Job run at

every 15 days
     intervalType = days, interval = 15, recurrence = true

every 1:30(AM) check kafka
    minute=30, hour=1, intervalType = time, recurrence = true

every 5 mins , check kafka
    minute = 5, recurrence = true, intervalType = time, recurrence = true

every month 3rd thursday , take database backup
    intervalType = months
    interval = 1
    weekdays : [Thursday],
    date: [15, 16, 17, 18, 19, 20, 21]
    recurrence = true


// beforeAll(async () => {
//     await db.query("CREATE TABLE students (id SERIAL PRIMARY KEY, name TEXT)");
// });
// beforeEach(async () => {
// // seed with some data
//     await db.query("INSERT INTO students (name) VALUES ('Elie'), ('Matt')");
// });

// afterEach(async () => {
//     await db.query("DELETE FROM students");
// });

// afterAll(async () => {
//     await db.query("DROP TABLE students");
//     db.end();
// });


if (!intervalType) {
            // If there is no interval then it repeats daily at the given second, minute, hour
            scheduleSpec = {};
            // @ts-ignore
            scheduleSpec.second = second ? second : 0;
            // @ts-ignore
            scheduleSpec.minute = minute ? minute : 0;
            // @ts-ignore
            scheduleSpec.hour = hour ? hour : 0;

        } else if (intervalType === "time") {
            // check if there is a last triggered time for this job
            let result = await scheduleModel.findOne({clientId: clientId, jobId: jobId}).exec();
            logger.info("result", result);
            // if intervalType is time, then it should repeat for every n hour + n minute + n second
            scheduleSpec = luxon.DateTime.local().setZone(timeZone).plus({
                seconds: second ? second : 0,
                minutes: minute ? minute : 0,
                hours: hour ? hour : 0,
            });

            // do {

            // } while()
            // @ts-ignore
            scheduleSpec = new Date(scheduleSpec);
        }


 else {
            // Reschedule the job
            // @ts-ignore
            let nextInvocationDate = luxon.DateTime.fromISO(fireDate).setZone(timeZone).plus({
                seconds: second ? second : 0,
                minutes: minute ? minute : 0,
                hours: hour ? hour : 0,
            });

            logger.info("in rescheduling block", second, minute, hour);
            // @ts-ignore
            job.reschedule(new Date(nextInvocationDate));
        }


    // job.on('run', async() => {
    //     logger.info("run event subscriber - ");
    //     // logger.info("job.triggeredJobs()", job.triggeredJobs());
    //     // // TODO: cancel the job if pending invocations lenght is 0
    //     // logger.info("job.pendingInvocations()", job.pendingInvocations());
    //     // // if (job.pendingInvocations().length === 0) {
    //     // //     job.cancel();
    //     // // }
    // });

    if (!job) return null;

    job.on('scheduled', (fireDate) => {
        console.log(fireDate);
        console.log(luxon.DateTime.fromISO(fireDate));
        // logger.info("scheduled event subscriber - ", luxon.DateTime.fromISO(fireDate).setZone(timeZone));
    });

    job.on('canceled', (fireDate) =>{
        logger.info("canceled event subscriber - ", fireDate._date.toISOString());
    });

// TODO: use cron builder for building a cron
    // // optionally, pass in a cron expression to override the default:
    // const cronExp = new cb('0 0 * * *')

    // TODO:- how to specify alternate days from input parameter.
    // solution:- use step value

    /**
     * 1. trigger a job once in 6 hour include minutes also and so on(interval)
     *      Input: minute: 30, hour: 6, interval: true, recurrence=true
     *      Output: cronRule "30 star/6 * * *" , for more https://crontab.guru/#30_star/6_*_*_* (replace star with * in link)
     * 2. trigger a job just once
     *      Input: minute: 30, hour: 6, dayOfMonth: 3, monthOfYear: 1, dayOfWeek: 0, recurrence: false(this is compulsory, it will repeat on the same period)
     *      Output: "30 6 3 1 0" , for more https://crontab.guru/#30_6_4_1_0
     * 3. triggger a job at might night
     *      Input: minute: 0, hour: 0, recurrence: false(if true , then it repeats on every same time period)
     *      Output: "0 0 * * *", for more https://crontab.guru/#0_0_*_*_*
     * 4. trigger job at every 1:30, this is trikcy use case.
     *      Solution: this is similar to the 1 case. No difference.
     * 5. trigger a job on alternate days, weeks, months(alternate days means it happens on one day, then happens on every second day after that. In the same way, something can happen in alternate weeks, years, or other periods of time.)
     *      For Alternate Days:
     *          Input: minute: 0, hour: 3, step: 2(means alternate days), recurrence: true
     *          Output: "0 3 star/setp(means 2 here) * *". for more https://crontab.guru/#0_3_star/2_*_*
     *      For Alternate Weeks:
     *          Input: minute: 0, hour: 3, step: 14(means alternate weeks), recurrence: true
     * 6. rescheduling of a job for every interval should continued with last triggered time.(for example, job should trigger at every 1.30, but when service got stopped, then need to make sure that it triggers on the correct seequence)
     * 7. handle scenarios with replicas.
     *      Solution: No
     */
    // interval = true means run a job at every n-hour:m-minutes gap/difference.
    // TODO: handling interval=true conditions becoming difficult , so will handle this once we do normal cron settings.
    // if (interval === true) {

    //     // verify highest weight value, order of weight is monthOfYear, dayOfMonth, hour, minute
    //     if (monthOfYear !== "*") {
    //         monthOfYear = `*/${monthOfYear};`
    //     } else if (dayOfMonth !== "*") {
    //         dayOfMonth = `*/${dayOfMonth}`;
    //     } else if (hour !== "*") {
    //         hour = `*/${hour}`;
    //     } else {
    //         minute = `*/${minute}`;
    //     }
    // }

    // if (recurrence === false) {
    //     const cronRule = `${second} ${minute} `;
    //     // validate cron rule
    //     if (cron(cronRule).isError()) {
    //         throw new Error(`Invalid cron ${cronRule}`);
    //     }
    //     return cronRule;
    // }

{
        "jobId": "8884cc39-ec0b-4d23-8257-a3917d6ae1dc",
        "nextInvocation": "2020-12-24T01:30:00.000Z",
        "pendingInvocations": 1,
        "triggeredJobs": 0
    }


"nativeProtocols": {
        "http:": {
          "METHODS": [
            "ACL",
            "BIND",
            "CHECKOUT",
            "CONNECT",
            "COPY",
            "DELETE",
            "GET",
            "HEAD",
            "LINK",
            "LOCK",
            "M-SEARCH",
            "MERGE",
            "MKACTIVITY",
            "MKCALENDAR",
            "MKCOL",
            "MOVE",
            "NOTIFY",
            "OPTIONS",
            "PATCH",
            "POST",
            "PRI",
            "PROPFIND",
            "PROPPATCH",
            "PURGE",
            "PUT",
            "REBIND",
            "REPORT",
            "SEARCH",
            "SOURCE",
            "SUBSCRIBE",
            "TRACE",
            "UNBIND",
            "UNLINK",
            "UNLOCK",
            "UNSUBSCRIBE"
          ],
          "STATUS_CODES": {
            "100": "Continue",
            "101": "Switching Protocols",
            "102": "Processing",
            "103": "Early Hints",
            "200": "OK",
            "201": "Created",
            "202": "Accepted",
            "203": "Non-Authoritative Information",
            "204": "No Content",
            "205": "Reset Content",
            "206": "Partial Content",
            "207": "Multi-Status",
            "208": "Already Reported",
            "226": "IM Used",
            "300": "Multiple Choices",
            "301": "Moved Permanently",
            "302": "Found",
            "303": "See Other",
            "304": "Not Modified",
            "305": "Use Proxy",
            "307": "Temporary Redirect",
            "308": "Permanent Redirect",
            "400": "Bad Request",
            "401": "Unauthorized",
            "402": "Payment Required",
            "403": "Forbidden",
            "404": "Not Found",
            "405": "Method Not Allowed",
            "406": "Not Acceptable",
            "407": "Proxy Authentication Required",
            "408": "Request Timeout",
            "409": "Conflict",
            "410": "Gone",
            "411": "Length Required",
            "412": "Precondition Failed",
            "413": "Payload Too Large",
            "414": "URI Too Long",
            "415": "Unsupported Media Type",
            "416": "Range Not Satisfiable",
            "417": "Expectation Failed",
            "418": "I'm a Teapot",
            "421": "Misdirected Request",
            "422": "Unprocessable Entity",
            "423": "Locked",
            "424": "Failed Dependency",
            "425": "Too Early",
            "426": "Upgrade Required",
            "428": "Precondition Required",
            "429": "Too Many Requests",
            "431": "Request Header Fields Too Large",
            "451": "Unavailable For Legal Reasons",
            "500": "Internal Server Error",
            "501": "Not Implemented",
            "502": "Bad Gateway",
            "503": "Service Unavailable",
            "504": "Gateway Timeout",
            "505": "HTTP Version Not Supported",
            "506": "Variant Also Negotiates",
            "507": "Insufficient Storage",
            "508": "Loop Detected",
            "509": "Bandwidth Limit Exceeded",
            "510": "Not Extended",
            "511": "Network Authentication Required"
          },
