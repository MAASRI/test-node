###

GET http://localhost:5002 HTTP/1.1

###

POST http://localhost:5002/schedules/delete HTTP/1.1
content-type: application/json

{
"clientId": "1234",
"scheduleIds": ["715e9a8a-0959-4874-be0c-6103f23d8223"]
}

###

POST http://localhost:5002/schedules/list HTTP/1.1
content-type: application/json

{
"clientId": "1234"
}

###

GET http://localhost:5002/jobs HTTP/1.1

###

POST http://localhost:5002/schedules/create HTTP/1.1
content-type: application/json

{
"clientId": "1234",
"scheduleName": "Weekly_Service10",
"requestingServiceName": "repoerter service10",
"requestURL": "https://api.github.com/users/bvenkatr",
"requestBody": {"name": "Venkat"},
"requestType": "POST",
"contentType": "application/json",
"minute": 1,
"intervalType":"time"
}

###

PUT http://localhost:5002/schedules/update HTTP/1.1
content-type: application/json

{
"clientId": "1234",
"scheduleId":"888d783d-000c-4565-aacb-eec1f7cc2972",
"requestingServiceName": "repoerter service10",
"requestType": "POST"
}

###
