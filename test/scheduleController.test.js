const request = require("supertest")("http://localhost:5002");
const luxon = require("luxon");


describe("GET /", () => {
    test("It should give welcome message", async () => {
        const response = await request.get("/");
        expect(response.body.message).toEqual("Welcome to Scheduler Service");
        // @ts-ignore
        expect(response.statusCode).toBe(200);
    });
});

describe("POST /schedules/list/", () => {
    test("get list of schedules", async () => {
        const response = await request.post("/schedules/list").send({ clientId: "1234", status: "deleted" });
        // console.log(response);
        expect(response.body.success).toEqual(true);
        // @ts-ignore
        expect(response.statusCode).toBe(200);
        expect(response.body.data.length).toBe(0);
    });
});

describe("POST /schedules/create", () => {
    test("with empty data{}", async () => {
        const response = await request.post("/schedules/create")
            .send({});
        expect(response.body.errors.length).toBeGreaterThanOrEqual(1);
        // @ts-ignore
        expect(response.statusCode).toBe(400);
    });

    let scheduleId;
    let clientId = "1234";
    test("with minimum data", async () => {
        let now = luxon.DateTime.local();
        let expected = now.plus({ minutes: 1 });
        let hour = expected.hour;
        let minute = expected.minute;
        const response = await request.post("/schedules/create")
            .send({
                "clientId": clientId,
                "scheduleName": "daily report scheduling",
                "requestingServiceName": "report-service",
                "requestType": "GET",
                "requestURL": "https://api.github.com/users/bvenkatr",
                "contentType": "application/json",
                "requestBody": "reprotName=Daily&format=pdf",
                "startDate": now.toFormat("yyyy-MM-dd"),
                "hour": hour,
                "minute": minute
            });
        expect(response.body.success).toBe(true);
        // @ts-ignore
        expect(response.statusCode).toBe(201);
        scheduleId = response.body.scheduleId;

        const allSchedules = await request.post("/schedules/list")
            .send({
                clientId
            });
        let requiredSchedule = allSchedules.body.data.filter(i => i.scheduleId === scheduleId);
        if (requiredSchedule.length === 0) {
            console.log("no schedules found found");
        }
        if (requiredSchedule.length > 1) {
            console.log("multiple schedules found, it is not expected behavior");
        }
        const jobInfo = await request.get("/jobs");
        let jobStatus = jobInfo.body.filter(i => i.jobId === requiredSchedule[0].jobId);
        expect(jobStatus[0].nextInvocation).toBe(expected.set({ second: 0, millisecond: 0 }).toUTC().toString());
    });
    test("with startDate in past with recurrence=true(default)", async () => {
        const response = await request.post("/schedules/create")
            .send({
                "clientId": "1234",
                "scheduleName": "daily report scheduling",
                "requestingServiceName": "report-service",
                "requestType": "GET",
                "requestURL": "https://api.github.com/users/bvenkatr",
                "contentType": "application/json",
                "requestBody": "reprotName=Daily&format=pdf",
                "startDate": "2020-12-18"
            });
        // @ts-ignore
        expect(response.statusCode).toBe(201);
        expect(response.body.success).toBe(true);
    });

    test("with startDate in past with recurrence=false", async () => {
        const response = await request.post("/schedules/create")
            .send({
                "clientId": "1234",
                "scheduleName": "daily report scheduling",
                "requestingServiceName": "report-service",
                "requestType": "GET",
                "requestURL": "https://api.github.com/users/bvenkatr",
                "contentType": "application/json",
                "requestBody": "reprotName=Daily&format=pdf",
                "startDate": "2020-12-18",
                "recurrence": false
            });
        // @ts-ignore
        expect(response.statusCode).toBe(500);
        expect(response.body.message).toBe("startDate is in past, so can't schedule a job");
    });

    test("recurrenceType=days", async () => {
        const response = await request.post("/schedules/create").send({
            "clientId": "1234",
            "scheduleName": "daily report scheduling",
            "requestingServiceName": "report-service",
            "requestType": "GET",
            "contentType": "application/json",
            "requestURL": "https://api.github.com/users/bvenkatr",
            "recurrenceType": "days"
        });

        // @ts-ignore
        expect(response.statusCode).toBe(201);
        expect(response.body.message).toBe("Successfully created the schedule");
    });
});

describe('Update schedule', () => {
    let now = luxon.DateTime.local(),
        expected = luxon.DateTime.local().plus({ days: 1 }),
        clientId = "1234",
        scheduleId,
        requestingServiceName = "report-service";
    test("create", async () => {
        const createScheduleResponse = await request.post("/schedules/create").send({
            clientId,
            "scheduleName": "daily report scheduling",
            requestingServiceName,
            "requestType": "GET",
            "requestURL": "https://api.github.com/users/bvenkatr",
            "contentType": "application/json",
            "requestBody": "reprotName=Daily&format=pdf",
            "startDate": now.toFormat("yyyy-MM-dd"),
        });
        expect(createScheduleResponse.body.success).toBe(true);
        // @ts-ignore
        expect(createScheduleResponse.statusCode).toBe(201);
        scheduleId = createScheduleResponse.body.scheduleId;
        const allSchedules = await request.post("/schedules/list")
            .send({
                clientId
            });
        let requiredSchedule = allSchedules.body.data.filter(i => i.scheduleId === scheduleId);
        expect(requiredSchedule.length).toBe(1);
        const jobInfo = await request.get("/jobs");
        let jobStatus = jobInfo.body.filter(i => i.jobId === requiredSchedule[0].jobId);
        expect(jobStatus[0].nextInvocation).toBe(expected.startOf("day").toUTC().toString());
    });
    test("update", async () => {
        const updateResponse = await request.put("/schedules/update").send({
            clientId, scheduleId, requestingServiceName
        });
        // @ts-ignore
        expect(updateResponse.statusCode).toBe(201);

        const allSchedules = await request.post("/schedules/list")
            .send({
                clientId
            });
        let requiredSchedule = allSchedules.body.data.filter(i => i.scheduleId === scheduleId);
        expect(requiredSchedule.length).toBe(1);
        const jobInfo = await request.get("/jobs");
        let jobStatus = jobInfo.body.filter(i => i.jobId === requiredSchedule[0].jobId);
        expect(jobStatus[0].nextInvocation).toBe(expected.startOf("day").toUTC().toString());
    });
    test("delete", async () => {
        const deleteResponse = await request.post("/schedules/delete").send({
            clientId,
            scheduleIds: [scheduleId]
        });
        // @ts-ignore
        expect(deleteResponse.statusCode).toBe(202);

        const allSchedules = await request.post("/schedules/list")
            .send({
                clientId
            });
        let requiredSchedule = allSchedules.body.data.filter(i => i.scheduleId === scheduleId);
        expect(requiredSchedule.length).toBe(0);
    });
});

describe("intervalType=days", () => {
    let now = luxon.DateTime.local(),
        expected = luxon.DateTime.local().plus({ days: 1 }),
        clientId = "1234",
        scheduleId,
        requestingServiceName = "report-service";
    test("create", async () => {
        const createScheduleResponse = await request.post("/schedules/create").send({
            clientId,
            "scheduleName": "daily report scheduling",
            requestingServiceName,
            "requestType": "GET",
            "requestURL": "https://api.github.com/users/bvenkatr",
            "contentType": "application/json",
            "requestBody": "reprotName=Daily&format=pdf",
            "intervalType": "days",
            "startDate": now.plus({ days: 1 }).toFormat("yyyy-MM-dd")
        });
        expect(createScheduleResponse.body.success).toBe(true);
        // @ts-ignore
        expect(createScheduleResponse.statusCode).toBe(201);
        scheduleId = createScheduleResponse.body.scheduleId;
        const allSchedules = await request.post("/schedules/list")
            .send({
                clientId
            });
        let requiredSchedule = allSchedules.body.data.filter(i => i.scheduleId === scheduleId);
        expect(requiredSchedule.length).toBe(1);
        const jobInfo = await request.get("/jobs");
        let jobStatus = jobInfo.body.filter(i => i.jobId === requiredSchedule[0].jobId);
        expect(jobStatus[0].nextInvocation).toBe(expected.startOf("day").toUTC().toString());
    });
    test("update", async () => {
        const updateResponse = await request.put("/schedules/update").send({
            clientId, scheduleId, requestingServiceName
        });
        // @ts-ignore
        expect(updateResponse.statusCode).toBe(201);

        const allSchedules = await request.post("/schedules/list")
            .send({
                clientId
            });
        let requiredSchedule = allSchedules.body.data.filter(i => i.scheduleId === scheduleId);
        expect(requiredSchedule.length).toBe(1);
        const jobInfo = await request.get("/jobs");
        let jobStatus = jobInfo.body.filter(i => i.jobId === requiredSchedule[0].jobId);
        expect(jobStatus[0].nextInvocation).toBe(expected.startOf("day").toUTC().toString());
    });
    test("delete", async () => {
        const deleteResponse = await request.post("/schedules/delete").send({
            clientId,
            scheduleIds: [scheduleId]
        });
        // @ts-ignore
        expect(deleteResponse.statusCode).toBe(202);

        const allSchedules = await request.post("/schedules/list")
            .send({
                clientId
            });
        let requiredSchedule = allSchedules.body.data.filter(i => i.scheduleId === scheduleId);
        expect(requiredSchedule.length).toBe(0);
    });
});

describe("requestBody value as object type", () => {
    let scheduleTime = luxon.DateTime.local().plus({ minutes: 1 }).set({ second: 0 }),
        expected = scheduleTime.toUTC().toString(),
        clientId = "1234",
        scheduleId,
        requestingServiceName = "report-service";
    test("create", async () => {
        const createScheduleResponse = await request.post("/schedules/create").send({
            clientId,
            "scheduleName": "Test schedule for requestBody value as object type",
            requestingServiceName,
            "requestType": "GET",
            "requestURL": "https://api.github.com/users/bvenkatr",
            "contentType": "application/json",
            "requestBody": { "reprotName": "Daily", "format": "pdf" },
            "recurrence": false,
            "minute": scheduleTime.minute,
            "hour": scheduleTime.hour,
        });
        expect(createScheduleResponse.body.success).toBe(true);
        // @ts-ignore
        expect(createScheduleResponse.statusCode).toBe(201);
        scheduleId = createScheduleResponse.body.scheduleId;
        const allSchedules = await request.post("/schedules/list")
            .send({
                clientId
            });
        let requiredSchedule = allSchedules.body.data.filter(i => i.scheduleId === scheduleId);
        expect(requiredSchedule.length).toBe(1);
        const jobInfo = await request.get("/jobs");
        let jobStatus = jobInfo.body.filter(i => i.jobId === requiredSchedule[0].jobId);
        expect(luxon.DateTime.fromISO(jobStatus[0].nextInvocation).set({ millisecond: 0 }).toUTC().toString()).toBe(luxon.DateTime.fromISO(expected).set({ millisecond: 0 }).toUTC().toString());
    });

    test("delete", async () => {
        const deleteResponse = await request.post("/schedules/delete").send({
            clientId,
            scheduleIds: [scheduleId]
        });
        // @ts-ignore
        expect(deleteResponse.statusCode).toBe(202);

        const allSchedules = await request.post("/schedules/list")
            .send({
                clientId
            });
        let requiredSchedule = allSchedules.body.data.filter(i => i.scheduleId === scheduleId);
        expect(requiredSchedule.length).toBe(0);
    });
});